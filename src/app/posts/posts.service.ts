import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {AngularFire} from 'angularfire2'; /// ex 7
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';

@Injectable()
export class PostsService {
 // private _url = 'http://jsonplaceholder.typicode.com/posts';    ex 7

 postsObservable;   // ex 7 
  getPosts(){
 //   return this._http.get(this._url).map(res => res.json()).delay(2000);   ex 7 // ex 7 class get the posts from firebase
 /*      delete this when we use "join" and instead writes whats below
   this.postsObservable = this.af.database.list('/posts');                   
   return this.postsObservable;     
*/  
            this.postsObservable = this.af.database.list('/posts').map(      /// use this for the join
      posts =>{
        posts.map(
          post => {
            post.userNames = [];
            for(var p in post.users){
                post.userNames.push(
                this.af.database.object('/users/' + p)
              )
            }
          }
        );
        return posts;
      }
    )
     return this.postsObservable;
 	}
  
    addPost(post){       // ex 7 class
    this.postsObservable.push(post);
  }
    updatePost(post){     ///// ex 7       after that changes in posts.component.ts
    let postKey = post.$key;
    let postData = {title:post.title, body:post.body};
    this.af.database.object('/posts/' + postKey).update(postData);
  }
    deletePost(post){                      //// ex 7     from posts.component.ts
    let postKey = post.$key;
    this.af.database.object('/posts/' + postKey).remove();
  }
  constructor(private af:AngularFire) { }    // it was private _http:Http before ex 7 

}
